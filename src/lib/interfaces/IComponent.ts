import type { Theme } from "../enums/Theme";

export interface IComponent {
  classes?: string;
  theme?: Theme;
}