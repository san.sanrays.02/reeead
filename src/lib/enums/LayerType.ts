// Добавлено поскольку у всех компонентов с одинаковым бэкграундом существуют небольшие отличия
// Тени, радиус скругления, padding и т.д. 
export enum LayerType {
  Heading = 'heading',
  Section = 'section',
  SectionAlt = 'sectionAlt',
  Card = 'card'
}